import curses
from curses import KEY_RIGHT, KEY_LEFT, KEY_UP, KEY_DOWN
import socket

teams = ['212.24.97.112']
port = 2727

def read_until(fd, ch):
	length = len(ch)
	received_data = ''
	while True:
		received_data += fd.recv(1)
		if received_data[-length:] == ch:
			break
	return received_data
	
def parse_game_responce(responce):
	return responce.replace(":", "").replace("COMMAND", "").replace("x=", "").replace("y=", "").strip().split(" ")
	
def play(fd):
	key = KEY_RIGHT
	command = "\'RIGHT\'"
	score = -1
	food = [0, 0]
	snake = [[4,10], [4,9], [4,8]]
	curses.initscr()
	curses.noecho()
	curses.curs_set(0)
	win = curses.newwin(20, 60, 0, 0)
	win.keypad(1)
	win.border(0)
	win.nodelay(1)
	while score < 28:
		data = parse_game_responce(read_until(fd, "\n"))
		
		if data[0] == "FOOD":
			score += 1
			food[0], food[1] = int(data[1]), int(data[2])
			win.addch(food[0], food[1], '*')
		
		elif data[0] == "SNAKE":
			if snake[0][0] != int(data[1]) or snake[0][1] != int(data[2]):
				break
				
			win.border(0)
			win.addstr(0, 2, 'Score : ' + str(score) + ' ')   
			win.addstr(0, 27, ' SNAKE ')                                   
			win.timeout(150 - (len(snake)/5 + len(snake)/10)%120)
			
			prevKey = key
			event = win.getch()
			key = key if event == -1 else event
			if key not in [KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, 27]:
				key = prevKey
			if key == KEY_RIGHT:
				command = "\'RIGHT\'"
			if key == KEY_LEFT:
				command = "\'LEFT\'"
			if key == KEY_UP:
				command = "\'UP\'"
			if key == KEY_DOWN:
				command = "\'DOWN\'"
			elif key == 27:
				fd.sendall("\'BREAK\'\n")
			fd.sendall(command + "\n")
			
			snake.insert(0, [snake[0][0] + (key == KEY_DOWN and 1) + (key == KEY_UP and -1), snake[0][1] + (key == KEY_LEFT and -1) + (key == KEY_RIGHT and 1)])
			if snake[0][0] == 0: snake[0][0] = 18
			if snake[0][1] == 0: snake[0][1] = 58
			if snake[0][0] == 19: snake[0][0] = 1
			if snake[0][1] == 59: snake[0][1] = 1
			
			if snake[0] != food:
				last = snake.pop()        
			win.addch(last[0], last[1], ' ')
			win.addch(snake[0][0], snake[0][1], '#')
			
		elif data[0] == "SCORE":
			curses.endwin()
			return int(data[1])
			
	curses.endwin()
	read_until(fd, "\n")
	fd.sendall("\'BREAK\'\n")
	return int(parse_game_responce(read_until(fd, "\n"))[1])
	
def attack(team, port):
	conn = socket.create_connection((team, port))
	print "".join([read_until(conn, '\n') for i in xrange(6)])
	conn.sendall("$play\n")
	score = play(conn)
	if score > 27:
		print read_until(conn, "\n")
	else:
		print "Fail to attack, try again, your score: %d" % score

for team in teams:
	attack(team, port)